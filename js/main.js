import {
  layThongTinTuForm,
  rendertasks,
  rendertasksCheck,
} from "./controllor.js";
// thêm
let arrTasks = [];
document.getElementById("addItem").addEventListener("click", () => {
  let tasks = layThongTinTuForm();
  // kt rỗng
  if (tasks == "") {
    Swal.fire("vui lòng không để rỗng");
    return;
  }

  arrTasks.push(tasks);
  rendertasks(arrTasks);
  // reset form
  document.getElementById("newTask").value = "";
});
//xóa
let Xoa = (text) => {
  let index = arrTasks.findIndex((Element) => {
    return Element == text;
  });
  arrTasks.splice(index, 1);
  rendertasks(arrTasks);
};

window.Xoa = Xoa;
// đánh dấu việc đã làm xong
let arrTasksCheck = [];
let check = (text) => {
  let index = arrTasks.findIndex((Element) => {
    return Element == text;
  });

  let arrCheck = [];
  let check = document.querySelectorAll(".checkfa");
  check.forEach((Element) => {
    arrCheck.push(Element);
  });
  if (arrTasks[index] == undefined) return;
  arrTasksCheck.push(arrTasks[index]);

  arrTasks.splice(index, 1);
  rendertasks(arrTasks);
  rendertasksCheck(arrTasksCheck);
};
window.check = check;
// sắp xếp a z
document.getElementById("two").addEventListener("click", () => {
  rendertasks(arrTasks.sort());
});
// sắp xếp z a
document.getElementById("three").addEventListener("click", () => {
  rendertasks(arrTasks.sort().reverse());
});
